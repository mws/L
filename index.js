const fs = require('fs-extra');
const pug = require('pug');
// Tools we need to do stuff

var settings = require('./config.json');
// 'urlgenmode': ['sequential'] | 'rand'
	// custom link name overrides this, and removes the link from sequential count
// 'linkmode': ['dir'] | 'html'
	// should the shortened url be /short/ or /short.html
// 'index': [false] | './path-to/index.pug'
// 'indexlist': [false] | true
	// List all urls shortened
// 'templatefile': './path-to/pug-file.pug'

// 'clean': [true] | false
	// remove previously existing redirect pages when making the new ones

var links = require('./links.json');
// Structure links as:
	// {'long':'https://example.com'},
	// or:
	// {'long':'https://example.com','custom':'your-custom-link-name'}

var urlsafechars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_-~';
// Needed for generating the short url

const pugtemplate = pug.compileFile(settings.templatefile);
// This'll be itteratively called with all of the links applied to it

if(settings.clean !== false){
	fs.removeSync('public/');
}
// Default to wiping the links folder

if(settings.linkmode === 'html'){
	var outpathfunction = (short) => `public/${short}.html`;
} else {
	var outpathfunction = (short) => `public/${short}/index.html`;
}
// Make a function for either directory or html short urls
	// https://stackoverflow.com/a/42280083/3196151

links.sort(function(linkone, linktwo) {
	if(linktwo.short){
		return 1;
	}
	return -1;
});
// Sort the links with the custom ones first
	// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/sort

var shortsalready = links.filter((array) => {
		if(array.short !== undefined){
			return array.short;
		}
	}); //https://stackoverflow.com/a/16037075/3196151
// Make a list of all of the currently custom ones
var sequentialvalue = 1;
for(var i = shortsalready.length; i < links.length; i++){
	var dupeshort = true;
	while(dupeshort){
		if(settings.urlgenmode === 'rand'){
			var madeshort = urlsafechars[Math.round((urlsafechars.length)*Math.random())] + '' + urlsafechars[Math.round((urlsafechars.length)*Math.random())];
		} else {
			var madeshort = urlsafechars[urlsafechars.length%sequentialvalue];
			sequentialvalue++;
		}
		if(shortsalready.indexOf(madeshort) === -1){
			dupeshort = false;
			shortsalready = shortsalready.concat(madeshort);
		}
	}
	links[i].short = madeshort;
}
// Make shorts for the remaining ones, checking for duplication

for(var i = 0; i<links.length; i++){
	var htmlcontent = pugtemplate({link: links[i].long});
	fs.outputFile(outpathfunction(links[i].short), htmlcontent);
	// https://github.com/jprichardson/node-fs-extra/blob/HEAD/docs/outputFile.md
}
// Actually make the files for the redirect

fs.writeFile('links.json', JSON.stringify(links, null, '	'));
if(settings.index){
	fs.outputFile('public/index.html', pug.renderFile(settings.index, {links:links, indexlist:settings.indexlist}));
}
// Default to having no index